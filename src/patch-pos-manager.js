const Orgchart = db.getMongo().getDB('spx-orgchart-readmodel').getCollection('orgcharts');
const Employee = db.getMongo().getDB('spx-employee-readmodel').getCollection('employees');
const EmployeeEvent = db.getMongo().getDB('spx-employee-eventstore').getCollection('events-employees');
const Code = db.getMongo().getDB('spx-employee-readmodel').getCollection('code-generates');
const EmployeeInCus = db.getMongo().getDB('spx-customer-readmodel').getCollection('employees');
const EmployeeInDemand = db.getMongo().getDB('spx-demand-readmodel').getCollection('employees');
const EmployeeInOrgchart = db.getMongo().getDB('spx-orgchart-readmodel').getCollection('employees');
const EmployeeInProperty = db.getMongo().getDB('spx-property-readmodel').getCollection('employees');
const EmployeeInSocial = db.getMongo().getDB('spx-social-readmodel').getCollection('employees');
const EmployeeInTrans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('employees');
const Tranx = db.getMongo().getDB('spx-property-readmodel').getCollection('primary-transactions');
const Trans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('transactions');
function log(obj){
	print(JSON.stringify(obj));
}

function updateManagerId() {
	const poses = Orgchart.find({managerId: { $exists: true, $nin: [null, '']},}, {id: 1, managerId: 1}).toArray();
	for (let i = 0; i < poses.length; i++) {
		const pos = poses[i];
		log(pos.managerId);
		let query = {id: pos.managerId};
		let model = {'$set': {managerAt: pos.id, workingAt: pos.id}};
		Employee.updateOne(query, model);
		EmployeeInCus.updateOne(query, model);
		EmployeeInOrgchart.updateOne(query, model);
		EmployeeInDemand.updateOne(query, model);
		EmployeeInProperty.updateOne(query, model);
		EmployeeInSocial.updateOne(query, model);
		EmployeeInTrans.updateOne(query, model);
	}
}

function main(){
	updateManagerId();
}
main();