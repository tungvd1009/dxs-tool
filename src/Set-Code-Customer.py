import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))

DemandCustomer=clientsource["msx-demand-readmodel"]["demand-customers_copy_dung"]
DemandCustomerUpdate=clientsource["msx-demand-readmodel"]["demand-customers_copy_dung"]
Customer=clientsource["msx-customer-readmodel"]["customers_copy_dung"]
Code=clientsource["msx-customer-readmodel"]["codes_copy_dung"]
Contract=clientsource["msx-contract-readmodel"]["contracts_copy_dung"]
CustomerUpdate=clientsource["msx-customer-readmodel"]["customers_copy_dung"]


# def GetCustomerExistsCode():
# #    docs = Customer.find({"$or":[ {"code":{"$ne":None}}]})
# #    docs = Customer.find({"code":{"$ne":None},"code":""})
    # # docs = Customer.find({"code":{"$ne":None},"code":{"$ne":""}})
    # docs = Customer.find({"code":{"$exists":True}})
    # return docs
    
def GetCustomerNotExistsCode():
    docs = Customer.find({ "$or" : [{"code":{"$exists":False}},{"code":{"$eq":""}} , {"code":{"$eq":None}}]})
    return docs
    
def GetCustomerDemandExistsCode():
 #    docs = DemandCustomer.find({"$or":[ {"code":{"$ne":None}}]})
 #   docs = DemandCustomer.find({"code":{"$ne":None},"code":""})
    docs = DemandCustomer.find({"code":{"$exists":True}})
    return docs

def GetCustomerDemandNotExistsCode():
#    docs = DemandCustomer.find({"$or":[ {"code":{"$ne":None}}]})
 #   docs = DemandCustomer.find({"code":{"$ne":None},"code":""})
    docs = DemandCustomer.find({ "$or" : [{"code":{"$exists":False}},{"code":{"$eq":""}} , {"code":{"$eq":None}}]})
    return docs    
    
def GetCustomerContractExistCode():
    docs = Contract.find({"mainCustomer.code":{"$exists":True}})
    return docs 

#, {"code":{'$eq':None}}
# def GetCustomerNew():
    # docs = Customer.find({"code":{"$exists":True}})
    # return docs

def Gencode(code):
    c = f'{code:010}'
    text = 'KH-'+c
    return text

    
    
def UpdateCustomerNoneCode(customer):
    query = {"id":customer["id"] }
    temp = Customer.find(query).count()
    if temp == 1:
        codeObj = Code.find_one_and_update({ 'name': 'contract.index' },{ '$set': { 'prefix': 'KH-', 'suffix': '' }, '$inc': { 'index': int(1) } },{ 'upsert': True})
        codenew = Code.find_one(codeObj)
        text=Gencode(codenew["index"])
        print(text)
        new = { "$set": { "code": text}}
        CustomerUpdate.update_one(query,new)
    
def UpdateCustomerDemandNoneCode(demandcustomer):
    query = {"id" : demandcustomer["id"]}
    temp = DemandCustomer.find(query).count()
    if temp == 1:
        codeObj = Code.find_one_and_update({ 'name': 'contract.index' },{ '$set': { 'prefix': 'KH-', 'suffix': '' }, '$inc': { 'index': int(1) } },{ 'upsert': True})
        codenew = Code.find_one(codeObj)
        text=Gencode(codenew["index"])
        new = { "$set": { "code": text}}
        print(text)
        DemandCustomerUpdate.update_one(query,new)
        

def UpdateMainCustomerContract(demand):
    query = {"mainCustomer.id" : demand["id"]}
    temp = Contract.find(query).count()
    if temp == 1:
        new = { "$set": { "mainCustomer.code": demand["code"]}}
        print("MainCustomer of Contract")
        print(demand["code"])
        Contract.update_one(query,new)

def UpdateCustomerContract(demand):
    query = {"mainCustomer.id" : demand["id"]}
    temp = Contract.find(query).count()
    if temp == 1:
        new = { "$set": { "customer.0.code": demand["code"]}}
        print("Customer of Contract")
        print(demand["code"])
        Contract.update_one(query,new)
    
    

def UpdateCustomer(mainCustomer):
    query = {"id" : mainCustomer["mainCustomer"]["id"] }
    temp = CustomerUpdate.find(query).count()
    if temp == 1:
        new = { "$set": { "code":  mainCustomer["mainCustomer"]["code"]}}
        print(mainCustomer["mainCustomer"]["code"])
        CustomerUpdate.update_one(query,new)
    

    
def main():
    
    # 1
    NoneDemandCustomers =  GetCustomerDemandNotExistsCode()
    print("Demand ko code")
    for demandcus in NoneDemandCustomers:
        UpdateCustomerDemandNoneCode(demandcus)
    #2
    Demandcustomers = GetCustomerDemandExistsCode()
    print("Demand có code")
    for demand in Demandcustomers:
        UpdateMainCustomerContract(demand)
        UpdateCustomerContract(demand)
    
    #3
    #Customer khong co code 
    Customers = GetCustomerNotExistsCode()
    for customer in Customers:
        UpdateCustomerNoneCode(customer)
        
    #4 
    #Customer co code trong Contract
    mainCutomers = GetCustomerContractExistCode()
    print("Customer có code trong Contract")
    for mainCutomer in mainCutomers:
        UpdateCustomer(mainCutomer)
        
   
        
main()