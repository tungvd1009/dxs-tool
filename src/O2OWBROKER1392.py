import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))


Property=clientsource["msx-property-readmodel"]["property-units"]
PropertyMatching=clientsource["msx-matching-readmodel"]["property-units_copy_dung"]



def GetProperty():
    docs = Property.find({"propertyStatus":{"$exists":True}})
    return docs


def UpdateProperty(property):
    query = {"unit.id" : property["id"] }
    new = { "$set": {"propertyStatus" : property["propertyStatus"] }}
    PropertyMatching.update_many(query,new)
    


def main():
    data = GetProperty()
    for property in data:
        print(property['id'])
        UpdateProperty(property)
main()