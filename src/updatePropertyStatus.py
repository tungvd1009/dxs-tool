import sys
import json
import pymongo
import shutil
from datetime import datetime, date

host = '192.168.0.12'
port = 62018


client = pymongo.MongoClient(host, int(port))

ContractQuery = client['msx-contract-readmodel']['contracts']
PropertyQuery = client['msx-property-readmodel']['property-units']

def getContracts():
    select = {'propertyUnitId':1, 'dealtDeposit': 1}
    query = {'dealtDeposit':{'$ne':None}}
    docs = ContractQuery.find(query)
    return docs
    
def updatePropertyStatusDeposit(id):
    query = {'id': id}
    new = { "$set": { "propertyStatus": 'deposit' }}
    PropertyQuery.update_one(query,new)

def main():
    contracts = getContracts()
    for contract in contracts:
       print(contract['propertyUnitId'])
       updatePropertyStatusDeposit(contract['propertyUnitId'])
main()