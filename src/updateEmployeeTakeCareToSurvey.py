import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = 'api.datxanh.online'
portsource = 62018
hostdest = 'api.datxanh.online'
portdest = 62018

clientsource = pymongo.MongoClient(hostsource, int(portsource))
clientdest = pymongo.MongoClient(hostdest, int(portdest))

DraftContractSource = clientsource['msx-contract-readmodel']['draft-contracts_copy_dung']
ContractSource = clientsource['msx-contract-readmodel']['contracts_copy_dung']
SurveySource = clientsource['msx-property-readmodel']['survey-property-units_copy_dung']
SurveySourceEvent = clientsource['msx-property-readmodel']['survey-property-units_copy_dung']
PropertySource = clientsource['msx-property-readmodel']['property-units']
email = 'autonvkd@automation.com'

def GetAllSurvey():
    query = {}
    docs = PropertySource.find(query)
    return docs
    
def GetProperty():
    query = { "employeeTakeCare" : {"$ne":None}}
    docs = PropertySource.find(query)
    return docs

def GetDraftContract():
    query = {"employeeTakeCare":{"$ne":None} , "propertyUnitId":{"$ne":None}}
    docs = DraftContractSource.find(query)
    return docs
    
def DelDraftContractById(id):
    DraftContractSource.delete_one({'_id': id})
    
def DelContractById(id):
    ContractSource.delete_one({'_id': id})

def GetContract():
    query = {"employeeTakeCare":{"$ne":None} , "propertyUnitId":{"$ne":None}}
    docs = ContractSource.find(query)
    return docs

def updateSurveyProperty(propertyUnitId, employeeTakeCare, pos):
    query = {'id':propertyUnitId}
    new = { '$set': { 'employeeTakeCare': employeeTakeCare , "pos": pos}}
    SurveySource.update_many(query,new)
    
def GetSurveyPropertyById(id):
    query = { "id" : id}
    docs = SurveySource.find_one(query)
    return docs

def updateContractByEmail(email):
    query = {"employeeTakeCare.email":email}
    docs = DraftContractSource.find(query)
    for doc in docs:
        property = GetSurveyPropertyById(doc['propertyUnit'])
        if property == None:
            print(doc['id'])
            DelDraftContractById(doc['id'])
    docs1 = ContractSource.find(query)
    for doc1 in docs1:
        print(doc1['id'])
        property = GetSurveyPropertyById(doc1['propertyUnit'])
        if property == None:
            print(doc1['id'])
            DelContractById(doc1['id'])

def main():
    # data1 = GetDraftContract()
    # for draft in data1:
       # updateSurveyProperty(draft['propertyUnitId'], draft['employeeTakeCare'], draft['pos'])
       
    # data2 = GetContract()
    # for contract in data2:
       # updateSurveyProperty(contract['propertyUnitId'], contract['employeeTakeCare'], contract['pos'])
       
    # data3 = GetProperty()
    # for property in data3:
       # updateSurveyProperty(property['id'], property['employeeTakeCare'], property['pos'])
    # updateContractByEmail(email)
    surveys = GetAllSurvey()
    for surveys in surveys:
main()