import sys
import json
import pymongo
import shutil
from datetime import datetime, date
from unidecode import unidecode
import unicodedata

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))


Property=clientsource["msx-property-readmodel"]["property-units_copy_dung"]
Matching=clientsource["msx-matching-readmodel"]["property-units-bk_copy"]
UnitAttribute=clientsource["msx-matching-readmodel"]["unit-attributes_copy_dung"]

def GetProperty():
    query = {'source' : "PKT" }
    docs = Property.find(query)
    return docs

def GetPropertyMatching():
    query = {'unit.id' : 'c22c1243-459d-484d-886e-b9325e7b98e2'}
    docs = Matching.find(query)
    return docs

def GetPropertyAttribute():
    query = {'unitId' : {"$ne":None} , 'key' : "location" }
    docs = UnitAttribute.find(query)
    return docs
    
def Compare(text1 ,text2):
    text1 = text1.replace(',','')
    text1 = unicodedata.normalize('NFC', text1)
    text2 = unicodedata.normalize('NFC', text2)
    if text2 in text1:
       result = "true"
    else:
       result = "fasle"
    return result

def CheckLocation(text):
#    b = b.replace(',','')
#    text = text.replace('70000','Hồ Chí Minh')
    text = text.replace('Vietnam','Việt Nam')
    return text

def UpdateAddressProperty(property):
    query = {"id" : property["id"] }
    a = 'Việt Nam'
    b = property["address"]
    result = Compare(b,a)
    if result == "fasle":
        c = b+", "+a
        new = { "$set": {"address" : c}}
        Property.update_many(query,new)

def UpdateAddressPropertyMatching(unit):
    query = {"unit.id" : unit["unit"]["id"] }
    a = 'Việt Nam'
    b = unit["unit"]["address"]
    print("b1 " + b)
    l = CheckLocation(b)
    print("b2 " + b)
    print("l1 " + l)
    result = Compare(l,a)
    print("l2 " + l)
    print("result " + result)
    if result == "fasle":
        c = l+", "+a
        new = { "$set": {"unit.address" : c}}
        # Matching.update_many(query,new)
    # if result == "true":
        # new = { "$set": {"unit.address" : l}}
        # Matching.update_many(query,new)
    print("c " + c)

def UpdateAddressUnitAttribute(attribute):
    query = {"unitId" : attribute["unitId"] ,'key' : "location" }
    a = 'Việt Nam'
    b = attribute["text"]
    l = CheckLocation(b)
    result = Compare(l,a)
    if result == "fasle":
        c = l+", "+a
        new = { "$set": {"text" : c}}
        UnitAttribute.update_many(query,new)
    if result == "true":
        new = { "$set": {"text" : l}}
        UnitAttribute.update_many(query,new)




def main():

    # propertys = GetProperty()
    # for property in propertys:
        # UpdateAddressProperty(property)
        # print(property["address"])
    units = GetPropertyMatching()
    for unit in units:
        print(unit["unit"]["address"])
        UpdateAddressPropertyMatching(unit)
    # attributes = GetPropertyAttribute()
    # for attribute in attributes:
        # UpdateAddressUnitAttribute(attribute)
        # print(attribute["unitId"])
main()