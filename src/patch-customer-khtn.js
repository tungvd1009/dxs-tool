const CustomerTable = db
  .getMongo()
  .getDB("msx-demand-readmodel")
  .getCollection("demand-customers");
const EmployeeTable = db
  .getMongo()
  .getDB("msx-demand-readmodel")
  .getCollection("employees");
function log(obj) {
  print(JSON.stringify(obj));
}
function findAllCustomer() {
  return CustomerTable.find().toArray();
}
function findIdEmployee(id) {
  return EmployeeTable.find({id});
}
function customerUpdateOne(cus) {
  return CustomerTable.update({id: cus.id}, {$set: {
    createdBy: cus.createdBy, 
    employee: cus.employee, 
    status: cus.status,
    createdDate: cus.createdDate,
  }}); 
}
function patchAllCustomer(customers) {
  for (let i = 0; i < customers.length; i++) {
    const cus = customers[i];
    if(!cus.createdDate){
      cus.createdDate = cus.updatedDate;
    }
    if(cus.employee && cus.employee.id){
      cus.createdBy = cus.employee.id;
    } else {
      cus.createdBy = cus.modifiedBy;
      const emp = findIdEmployee(cus.modifiedBy);
      if(emp.id){
        cus.employee = cus.emp;
      } else {
        cus.employee = null;
      }
    }
	cus.status = 1;
    log(cus.id);
    customerUpdateOne(cus);
  }
}

function main() {
  const customers = findAllCustomer();
  patchAllCustomer(customers);
}
main();
