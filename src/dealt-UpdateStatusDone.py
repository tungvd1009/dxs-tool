import sys
import json
import pymongo
import shutil
from datetime import datetime, date

# host = 'api.datxanh.online'
# port = 62018


client = pymongo.MongoClient(host, int(port))

DealtQuery = client['msx-dealt-readmodel']['dealts_copy']
ContractQuery = client['msx-contract-readmodel']['contracts']

def getContractDone():
    query = {'state':'LIQUIDATE', 'status':'DONE', 'dealtDeposit':{'$ne': None}}
    docs = ContractQuery.find(query)
    return docs
    
def updateContractInDealt(contract):
    query = {'code': contract['dealtDeposit'], 'state' : 'in_scope', 'status':'done', 'contractId':contract['id']}
    new = { "$set": { "contract": contract }}
    DealtQuery.update_one(query,new)

def main():
    contracts = getContractDone()
    for contract in contracts:
       print(contract['id'])
       updateContractInDealt(contract)
# test: {'contract.state':'LIQUIDATE', 'contract.status':'DONE'}
main()