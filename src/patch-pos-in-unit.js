// const dbProperty = 'msx-property-readmodel';
// const dbOrgchart = 'msx-orgchart-readmodel';
const colTicketName = 'primary-transactions';
const colUnitName = 'property-units';
const colOrgchartName = 'orgcharts';
const khoSpId = '16d606ec-12e0-42dd-8aa4-305efc3fb488'; //KHO SAN PHAM
const UnitCol = db.getMongo().getDB(dbProperty).getCollection(colUnitName);//ticket collection
const TicketCol = db.getMongo().getDB(dbProperty).getCollection(colTicketName);//ticket collection
const OrgChartCol = db.getMongo().getDB(dbOrgchart).getCollection(colOrgchartName); //orgchart collection

function log(obj){
	print(JSON.stringify(obj));
}

function findTicketSucceed(unit){
	const ids = unit.priorities.map(t => t.id);
	// log(ids);
	return TicketCol.findOne({
		id: {$in: ids},
		status: "SUCCESS",
		'propertyUnit.id': unit.id
	});
}

function main(){
	//find all unit success, pos in 'KHO SAN PHAM'
	const units = UnitCol.find({
		"primaryStatus" : "SUCCESS",
		'pos.id': khoSpId,
		$expr: {$gt: [{"$size": ["$priorities"]}, 0]}
	}).toArray();

	//patch unit.pos = ticket.pos
	for (let i = 0; i < units.length; i++) {
		const unit = units[i];
		log(unit.id);
		const ticket = findTicketSucceed(unit);
		if(ticket){
			const pos = OrgChartCol.findOne({id: ticket.pos.id}, {
				"parentId" : 1,
				"_id" : 1,
				"name" : 1,
				"id" : 1,
				"staffIds" : 1,
				"devBusinessIds" : 1,
				"saleAdminIds" : 1
			});
			UnitCol.updateOne({id: unit.id}, {$set: {pos: pos}});
		}
	}
}
main();