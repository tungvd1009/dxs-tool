import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = 'api.datxanh.online'
portsource = 62018
hostdest = 'api.datxanh.online'
portdest = 62018
strFromDate = ''
strEndDate = ''

if strFromDate == "" or strEndDate == "":
    fromDate = datetime.strptime("1/1/1996","%d/%m/%Y")
    endDate = datetime.now()
else:
    fromDate = datetime.strptime(sys.argv[5], "%d/%m/%Y")
    endDate = datetime.strptime(sys.argv[6], "%d/%m/%Y")

clientsource = pymongo.MongoClient(hostsource, int(portsource))
clientdest = pymongo.MongoClient(hostdest, int(portdest))

LeadSource = clientsource['msx-lead-readmodel']['leads_copy_dung']
LeadDest = clientdest['msx-lead-readmodel']['leads_copy_dung']

def GetCustomer():
    LeadSource = clientsource["msx-lead-readmodel"]["leads_copy_dung"]
    select = { "customer.personalInfo.lastName":1 ,"customer.personalInfo.firstName":1}
    query = {"source":"MKT"}
    docs = LeadSource.find(query, select)
    return docs

def GetCustomer2():
    LeadSource = clientsource["msx-lead-readmodel"]["leads_copy_dung"]
    select = { "lastName":1 ,"firstName":1}
    query = {"source":{"$ne":"MKT"},"lastName":{"$exists":True}}
    docs = LeadSource.find(query, select)
    return docs

def Lead(user):
    LeadDest = clientdest['msx-lead-readmodel']['leads_copy_dung']
    docs = LeadDest.find({"id":user["_id"]})
    a = user["customer"]["personalInfo"]["lastName"]
    b = user["customer"]["personalInfo"]["firstName"]
    c = a +" "+b
    query = {"id":user["_id"]}
    new = { "$set": { "customer.personalInfo.name": c }}
    new2 = { "$set": { "name" : c}}
    LeadDest.update_many(query,new)
    LeadDest.update_many(query,new2)
    return Lead
def Lead2(user2):
    LeadDest = clientdest['msx-lead-readmodel']['leads_copy_dung']
    docs = LeadDest.find({"id":user2["_id"]})
    a = user2["lastName"]
    b = user2["firstName"]
    c = a +" "+b
    query = {"id":user2["_id"]}
    new = { "$set": { "name": c }}
    LeadDest.update_many(query,new)
    return Lead2
def main():
    data = GetCustomer()
    dist = GetCustomer2()
    for user in data:
       print(user)
       Lead(user)
    for user2 in dist:
       Lead2(user2)
main()