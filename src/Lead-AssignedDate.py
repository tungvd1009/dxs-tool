import sys
import json
import pymongo
import shutil
from datetime import datetime, date

host = 'api.datxanh.online'
port = 62018


client = pymongo.MongoClient(host, int(port))

LeadEvent = client['msx-lead-eventstore']['events-leads']
LeadQuery = client['msx-lead-readmodel']['leads_copy_dung']
LeadHistoryQuery = client['msx-lead-readmodel']['lead-histories_copy_dung']

def getLeadPulled():
    select = {'eventName':1, 'streamId':1, 'commitStamp': 1}
    query = {'eventName':'leadAssigned'}
    sort = [("eventName",pymongo.ASCENDING), ("commitStamp",pymongo.ASCENDING)]
    docs = LeadEvent.find(query, select).sort(sort)
    return docs
    
def updateLeadAssignedDate(id, assignedDate):
    query = {"id":id, 'lifeCycleStatus':{'$ne': 'inpool'}}
    new = { "$set": { "assignedDate": assignedDate }}
    LeadQuery.update_one(query,new)
    
def updateLeadHistoryAssignedDate(id, assignedDate):
    query = {"id":id, 'lifeCycleStatus':{'$ne': 'inpool'}}
    new = { "$set": { "assignedDate": assignedDate }}
    LeadHistoryQuery.update_one(query,new)


def main():
    leads = getLeadPulled()
    for lead in leads:
       print(lead['streamId'])
       updateLeadHistoryAssignedDate(lead['streamId'], lead['commitStamp'])
       updateLeadAssignedDate(lead['streamId'], lead['commitStamp'])
main()