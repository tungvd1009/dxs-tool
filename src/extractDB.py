import sys
import json
import pymongo
import shutil
dbhost='api.datxanh.online'
dbport=62018
client = pymongo.MongoClient(dbhost, dbport)
colSrc = client['msx-property-eventstore']['events-consignments']
colDist = client['Warhouse']['events-consignments']
filter = {
    'payload.employeeTakeCare.id' : 1,
    'payload.resource' : 1,
    'payload.status' : 1,
    'payload.type' : 1,
    'payload.ticketId' : 1,
    'payload.pos.id' : 1,
    'payload.pos.name' : 1,
    'commitStamp' : 1,
    'eventName' : 1
}
def main():
    docs = colSrc.find({}, filter).limit(2)
    colDist.insert_many(docs)
main()