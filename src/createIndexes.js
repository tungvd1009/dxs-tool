const Tranx = db.getMongo().getDB('msx-property-readmodel').getCollection('primary-transactions');//ycdch,ycdco
const EventProject = db.getMongo().getDB('msx-property-readmodel').getCollection('event-projects');//ycdch,ycdco
const Unit = db.getMongo().getDB('msx-property-readmodel').getCollection('property-units');//ycdch,ycdco
const Project = db.getMongo().getDB('msx-property-readmodel').getCollection('projects');//ycdch,ycdco
const Transaction = db.getMongo().getDB('msx-transaction-readmodel').getCollection('transactions');//phieuthu
const UnitEmploye = db.getMongo().getDB('msx-property-readmodel').getCollection('employees');//phieuthu
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	// Tranx.createIndexes([
		// {'_id' : 1},
		// {'id' : 1},
		// {'code' : 1},
		// {'propertyUnit.id' : 1},
		// {'propertyUnit.code' : 1},
		// {'project.id' : 1},
		// {'pos.id' : 1},
		// {'customer.personalInfo.phone' : 1},
		// {'customer.personalInfo.name' : 1},
		// {'bookingTicketCode' : 1},
		// {'escrowTicketCode' : 1},
		// {'reciept.code' : 1},
		// {'priority' : 1},
		// {'status' : 1},
		// {'employee.id' : 1},
		// {'createdDate' : 1},
		// {'updatedDate' : 1},
		// {'modifiedDate' : 1},
		// {'systemnoErp' : 1},
		// {'codeCustomerErp' : 1},
	// ]);
	Unit.createIndexes([
		{'_id' : 1},
		{'id' : 1},
		{'code' : 1},
		{'project.id' : 1},
		{'pos.id' : 1},
		{'category.id' : 1},
		{'priorities.bookingTicketCode' : 1},
		{'priorities' : 1},
		{'priorities.posId' : 1},
		{'priorities.id' : 1},
		{'priorities.status' : 1},
		{'priorities.priority' : 1},
		{'primaryStatus' : 1},
		{'createdDate' : 1},
		{'updatedDate' : 1},
		{'modifiedDate' : 1},
		{'block' : 1},
		{'floor' : 1},
		{'lot' : 1},
		{'quarter' : 1},
		{'extendable' : 1},
		{'extendPos' : 1},
		{'extendPos.id' : 1},
		{'registeredPos.id' : 1},
		{'stage' : 1},
	]);
	// Project.createIndexes([
		// {'_id' : 1},
		// {'id' : 1},
		// {'code' : 1},
	// ]);
	// UnitEmploye.createIndexes([
		// {'_id' : 1},
		// {'id' : 1},
	// ]);
	// EventProject.createIndexes([
		// {'projectId' : 1},
		// {'project.id' : 1},
		// {'status' : 1},
		// {'stageStatus' : 1},
		// {'priorityStatus' : 1},
		// {'stage' : 1},
	// ]);
	// Transaction.createIndexes([
		// {'_id' : 1},
		// {'id' : 1},
		// {'createdById' : 1},
		// {'posId' : 1},
		// {'pos.id' : 1},
		// {'pos.parentId' : 1},
		// {'propertyTicket.project.id' : 1},
		// {'propertyTicket.status' : 1},
		// {'propertyTicket.bookingTicketCode' : 1},
		// {'propertyTicket.escrowTicketCode' : 1},
		// {'propertyTicket.ticketType' : 1},
		// {'customer.personalInfo.phone' : 1},
		// {'customer.personalInfo.name' : 1},
		// {'transaction.propertyTicket.project.id' : 1},
		// {'transaction.propertyTicket.status' : 1},
		// {'transaction.propertyTicket.bookingTicketCode' : 1},
		// {'transaction.propertyTicket.escrowTicketCode' : 1},
		// {'transaction.propertyTicket.ticketType' : 1},
		// {'transaction.customer.personalInfo.phone' : 1},
		// {'transaction.customer.personalInfo.name' : 1},
		// {'receiptDate' : 1},
		// {'receiptNum' : 1},
		// {'propertyTicket.systemnoErp' : 1},
	// ]);
}
main();