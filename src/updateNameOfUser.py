import sys
import json
import pymongo
import shutil
from datetime import datetime, date

host = 'api.datxanh.online'
port = 62018


client = pymongo.MongoClient(host, int(port))

EmployeeQuery = client['msx-employee-readmodel']['employees']
UserQuery = client['msx-sts-readmodel']['users_copy_dung']

def getEmployee():
    select = {'id':1, 'name': 1, 'email':1}
    query = {}
    docs = EmployeeQuery.find(query)
    return docs
    
def updateNameInUser(id, name):
    query = {'id': id}
    new = { "$set": { "name": name }}
    UserQuery.update_one(query,new)

def main():
    emps = getEmployee()
    for emp in emps:
       print(emp['email'])
       updateNameInUser(emp['id'], emp['name'])
main()