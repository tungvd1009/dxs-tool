import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]
hostdest = sys.argv[3]
portdest = sys.argv[4]
strFromDate = sys.argv[5]
strEndDate = sys.argv[6]

if strFromDate == "" or strEndDate == "":
    fromDate = datetime.strptime("1/1/1996","%d/%m/%Y")
    endDate = datetime.now()
else:
    fromDate = datetime.strptime(sys.argv[5], "%d/%m/%Y")
    endDate = datetime.strptime(sys.argv[6], "%d/%m/%Y")

clientsource = pymongo.MongoClient(hostsource, int(portsource))
clientdest = pymongo.MongoClient(hostdest, int(portdest))

LeadSource = clientsource['msx-lead-readmodel']['leadsdemo']
LeadDest = clientdest['msx-lead-readmodel']['leadsdemo']

def GetCustomer():
    LeadSource = clientsource["msx-lead-readmodel"]["leadsdemo"]
    select = { "customer.personalInfo.lastName":1 ,"customer.personalInfo.firstName":1}
    query = {"source":"MKT","lastName"{"$ne":None} , "firstName"{"$ne":None}}
    docs = LeadSource.find(query, select)
    return docs

def GetCustomer2():
    LeadSource = clientsource["msx-lead-readmodel"]["leadsdemo"]
    select = { "lastName":1 ,"firstName":1}
    query = {"source":{"$ne":"MKT"},"lastName":{"$exists":True}}
    docs = LeadSource.find(query, select)
    return docs

def GetCustomer3():
    LeadSource = clientsource["msx-lead-readmodel"]["leadsdemo"]
    select = { "lastName":1 ,"firstName":1}
    query = {"source":{"$ne":"MKT"},"fristName":{"$exists":True}}
    docs = LeadSource.find(query, select)
    return docs
    
def Lead(user):
    LeadDest = clientdest['msx-lead-readmodel']['leadsdemo']
    docs = LeadDest.find({"id":user["_id"]})
    a = user["customer"]["personalInfo"]["lastName"]
    b = user["customer"]["personalInfo"]["firstName"]
    c = a +" "+b
    query = {"id":user["_id"]}
    new = { "$set": { "customer.personalInfo.name": c }}
    new2 = { "$set": { "name" : c}}
    LeadDest.update_many(query,new)
    LeadDest.update_many(query,new2)


def Lead2(user2):
    LeadDest = clientdest['msx-lead-readmodel']['leadsdemo']
    docs = LeadDest.find({"id":user2["_id"]})
    a = user2["lastName"]
    b = user2["firstName"]
    c = a +" "+b
    query = {"id":user2["_id"]}
    new = { "$set": { "name": c }}
    LeadDest.update_many(query,new)


def Lead3(user3):
    LeadDest = clientdest['msx-lead-readmodel']['leadsdemo']
    docs = LeadDest.find({"id":user3["_id"]})
    a = user3["lastName"]
    b = user3["firstName"]
    c = a +" "+b
    query = {"id":user3["_id"]}
    new = { "$set": { "name": c }}
    LeadDest.update_many(query,new)


def main():
    data = GetCustomer()
    data2 = GetCustomer2()
    data3 = GetCustomer3()
    for user in data:
       Lead(user)
    for user2 in data2:
       Lead2(user2)
    for user3 in data3:
       Lead3(user3)
main()