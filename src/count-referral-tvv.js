const Employee = db.getMongo().getDB('spx-employee-readmodel').getCollection('employees');
function log(obj){
	print(JSON.stringify(obj));
}
function main(){
	const employeeReferrals = Employee.aggregate([
			{
				$match: {'referralPerson.id': {$nin: [null, '']}}
			},
			{
				$group: {
					_id: '$referralPerson.id',
					totalReferrals: {$sum: 1}
				}
			}
			]).toArray();
	for (let i = 0; i < employeeReferrals.length; i++) {
		const emp = employeeReferrals[i];
		log('emp id : ' + emp._id);
		Employee.updateOne({id: emp._id}, {$set: {totalReferrals: emp.totalReferrals}});
	}
}
main();