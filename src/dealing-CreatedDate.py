import sys
import json
import pymongo
import shutil
from datetime import datetime, date

host = 'api.datxanh.online'
port = 62018


client = pymongo.MongoClient(host, int(port))

DealingEvent = client['msx-matching-eventstore']['events-dealings']
DealingQuery = client['msx-matching-readmodel']['dealings']

def getListDealing():
    select = {'eventName':1, 'streamId':1, 'commitStamp': 1}
    query = {}
    docs = DealingEvent.find(query, select)
    return docs
    
def updateDateForDealing(id, commitStamp):
    query = {"id":id, "createdDate": {"$eq":None}, "createdDate": {"$exists":False}}
    new = { "$set": { "createdDate": commitStamp }}
    DealingQuery.update_one(query,new)

def main():
    dealings = getListDealing()
    for dealing in dealings:
       print(dealing['streamId'])
       updateDateForDealing(dealing['streamId'], dealing['commitStamp'])
main()