const Employee = db.getMongo().getDB('spx-employee-readmodel').getCollection('employees');
const EmployeeEvent = db.getMongo().getDB('spx-employee-eventstore').getCollection('events-employees');
const Code = db.getMongo().getDB('spx-employee-readmodel').getCollection('code-generates');
const EmployeeInCus = db.getMongo().getDB('spx-customer-readmodel').getCollection('employees');
const EmployeeInDemand = db.getMongo().getDB('spx-demand-readmodel').getCollection('employees');
const EmployeeInOrgchart = db.getMongo().getDB('spx-orgchart-readmodel').getCollection('employees');
const EmployeeInProperty = db.getMongo().getDB('spx-property-readmodel').getCollection('employees');
const EmployeeInSocial = db.getMongo().getDB('spx-social-readmodel').getCollection('employees');
const EmployeeInTrans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('employees');
const Tranx = db.getMongo().getDB('spx-property-readmodel').getCollection('primary-transactions');
const Trans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('transactions');
function log(obj){
	print(JSON.stringify(obj));
}
function generateEmpCode(name, prefix) {
    let index = 100;
    let rs = Code.findOneAndUpdate(
        { name: name, prefix: prefix },
        { $set: { prefix: prefix }, $inc: { index: 1 } },
        { returnNewDocument: true });
    if (rs === null) {
        Code.insert({_id: uuidv4(), name: name, prefix: prefix, index: index});
    } else {
        index = parseInt(rs.index);
    };
    const pad = "0000000000";
    const str = (index).toString();
    return `${prefix}${pad.substring(0, pad.length - str.length)}${str}`;
}
function updateEmpCode(empId, code) {
	Employee.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeEvent.updateMany({'payload.id': empId}, {$set: {'payload.code' : code, 'payload.promotionCode': code}});
	//update employee other database
	EmployeeInCus.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeInDemand.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeInOrgchart.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeInProperty.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeInSocial.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	EmployeeInTrans.updateOne({id: empId}, {$set: {code : code, promotionCode: code}});
	
}
function updateEmpCodeRefer(empId, code) {
	Tranx.updateMany({'employee.id': empId}, {$set: {'employee.code' : code}});
	Trans.updateMany({'propertyTicket.employee.id': empId}, {$set: {'propertyTicket.employee.code' : code}});
	Trans.updateMany({'transaction.propertyTicket.employee.id': empId}, {$set: {'transaction.propertyTicket.employee.code' : code}});
}
function main(){
	const employees = Employee.find({}, {id: 1, code: 1}).toArray();
	for (let i = 0; i < employees.length; i++) {
		const emp = employees[i];
		if(emp.id && emp.id!=''){
			log('before ' + emp.code);
			const code = generateEmpCode('employee.index', 'PROPCOM-');
			updateEmpCode(emp.id, code);
			updateEmpCodeRefer(emp.id, code);
			log('after ' + code);
		}
	}
}
main();