// const dbName = "msx-demand-readmodel";
const CustomerTable = db
  .getMongo()
  .getDB(dbName)
  .getCollection("demand-customers");
const EmployeeTable = db
  .getMongo()
  .getDB(dbName)
  .getCollection("employees");
function log(obj) {
  print(JSON.stringify(obj));
}

function deleteAllCustomerInvalid() {
  return CustomerTable.deleteMany({
    $expr: {
      $and: [
        {$lte: ["$employee.id", null]},
        {$lte: ["$createdBy", null]}
      ]
    }
  }); 
}
function patchAllCustomer() {
  return CustomerTable.aggregate([
    {
      $match: {
        $expr: {
          $or: [
            { $lte: ["$employee.id", null] },
            { $lte: ["$createdBy", null] }
          ]
        }
      }
    },
    {
      $lookup: {
        from: "employees",
        let: { createdBy: "$createdBy", employeeId: "$employee.id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $or: [
                  { $eq: ["$id", "$$createdBy"] },
                  { $eq: ["$id", "$$employeeId"] }
                ]
              }
            }
          },
          {
            $project: {
              id: 1,
              name: 1,
              code: 1,
              email: 1,
              "pos.id": 1,
              "pos.name": 1,
              "pos.code": 1,
              "pos.parentId": 1
            }
          }
        ],
        as: "employeeData"
      }
    },
    { $unwind: "$employeeData" },
    {
      $addFields: {
        createdBy: {
          $cond: [{ $lte: ["$createdBy", null] }, "$employee.id", "$createdBy"]
        },
        employee: "$employeeData"
      }
    },
    {
      $project: { employeeData: 0 }
    },
    { $out: "demand-customers" }
  ]);
}

function main() {
  deleteAllCustomerInvalid();
  patchAllCustomer();
}
main();
