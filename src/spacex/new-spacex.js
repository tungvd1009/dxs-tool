// const dbProperty = 'msx-property-readmodel';
// const colProperty = 'primary-transactions';
// const dbOrgchart = 'msx-orgchart-readmodel';
// const colOrgchart = 'orgcharts';
const TicketCol = db.getMongo().getDB(dbProperty).getCollection(colProperty);//ticket collection
const OrgChartCol = db.getMongo().getDB(dbOrgchart).getCollection(colOrgchart); //orgchart collection

function log(obj){
	print(JSON.stringify(obj));
}
function getSanByPos(pos) {
	if(pos && pos.parentId && pos.type && pos.type === 'POS'){
		return OrgChartCol.findOne({id: pos.parentId}, {code: 1, parentId:1, name: 1, id: 1, type: 1, staffIds: 1});
	}
	return pos;
}

function patchTicket(project){
	let list = TicketCol.find({'pos.type': 'POS'}).toArray();
	for (let i = 0; i < list.length; i++) {
		const ticket = list[i];
    const pos = getSanByPos(ticket.pos);
    log(ticket.id)
		TicketCol.updateOne({
      'id': ticket.id
    }, {
      $set: {pos: pos}
    });
	}
}


function main(){
	patchTicket();
}
main();