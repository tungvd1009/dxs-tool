import { Document } from "mongoose";
import { ISample } from "./base.interface";

export interface ISampleDocument extends Document, ISample {
  id: string;
}
