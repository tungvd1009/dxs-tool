import { Model } from "mongoose";
import { Inject, Injectable } from "@nestjs/common";
import { ISampleDocument } from "../interfaces/document.interface";
import { CommonConst } from "../../shared/constant/index";
import { ISample } from "../interfaces/base.interface";

@Injectable()
export class SampleExportRepository {
  constructor(
    @Inject(CommonConst.SAMPLE_QUERY_MODEL_TOKEN)
    private readonly readModel: Model<ISampleDocument>
  ) {}

  async find(query: any, projection: any = {}): Promise<ISample[]> {
    return await this.readModel.find(query, projection).lean();
  }

  async findOne(query: any, projection: any = {}): Promise<ISample> {
    return await this.readModel.findOne(query, projection).lean();
  }
}
