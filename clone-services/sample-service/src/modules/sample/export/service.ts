import { Injectable } from "@nestjs/common";
import { SampleExportRepository } from "./repository";

@Injectable()
export class SampleExportService {
  private readonly context = SampleExportService.name;

  constructor(private readonly repository: SampleExportRepository) {}

  async find(query: any) {
    return await this.repository.find(query);
  }

  async findOne(id: string) {
    return await this.repository.findOne({ id });
  }
}
