import { Module } from "@nestjs/common";
import { QueryDatabaseModule } from "../../database/query/query.database.module";
import { AuthModule } from "../../auth/auth.module";
import { SampleExportService } from "./service";
import { SampleExportRepository } from "./repository";
import { LoggerModule } from "../../logger/logger.module";
import { MgsSenderModule } from "../../mgs-sender/mgs-sender.module";
import { ConfigModule } from "../../config/config.module";
import { SampleProviders } from "../infra/providers";

@Module({
  imports: [
    QueryDatabaseModule,
    AuthModule,
    MgsSenderModule,
    LoggerModule,
    ConfigModule,
  ],
  providers: [...SampleProviders, SampleExportRepository, SampleExportService],
  exports: [SampleExportService],
})
export class SampleExportModule {}
