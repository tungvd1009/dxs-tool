import { Controller, Get, Param, Query, UseInterceptors } from "@nestjs/common";
import {
  ApiUseTags,
  ApiImplicitQuery,
  ApiOperation,
  ApiImplicitParam,
} from "@nestjs/swagger";
import { SampleService } from "../application/service";
import { LoggingInterceptor } from "../../../common/interceptors/logging.interceptor";

@ApiUseTags("[Public] Sample - API public")
@Controller("v1/public/sample")
@UseInterceptors(LoggingInterceptor)
export class SamplePublicController {
  constructor(private readonly service: SampleService) {}

  @ApiOperation({ title: "Find all public" })
  @ApiImplicitQuery({
    name: "page",
    required: true,
    description: "[Paging] page",
  })
  @ApiImplicitQuery({
    name: "pageSize",
    required: true,
    description: "[Paging] pageSize",
  })
  @ApiImplicitQuery({
    name: "sort",
    required: false,
    isArray: true,
    description: "[Sort] sort",
  })
  @ApiImplicitQuery({
    name: "q",
    required: false,
    description: "[Filter] Search text",
  })
  @Get()
  findPublicAll(@Query() query?: any) {
    return this.service.findPublicAll({ ...query });
  }

  @ApiOperation({ title: "Find one public" })
  @ApiImplicitParam({ name: "id", required: true, description: "Id" })
  @Get(":id")
  findPublicById(@Param("id") id: string) {
    return this.service.findPublicById(id);
  }
}
