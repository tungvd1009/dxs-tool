import { pick } from "lodash";
import { ISample } from "../../interfaces/base.interface";

export class SamplePagingResponseDto {
  rows: SampleResponseDto[];
  total: number;
  page: number;
  pageSize: number;
  totalPages: number;

  constructor(init?: Partial<SamplePagingResponseDto>) {
    Object.assign(
      this,
      pick(init, ["rows", "total", "page", "pageSize", "totalPages"])
    );
  }
}

export class SampleResponseDto {
  id: string;
  createdDate: Date;
  createdBy: string;
  modifiedDate: Date;
  modifiedBy: string;
  active: string;
  // Custom fields

  constructor(init?: Partial<SampleResponseDto | ISample>) {
    Object.assign(
      this,
      pick(init, [
        "id",
        "createdDate",
        "createdBy",
        "modifiedDate",
        "modifiedBy",
        "active",
        // Custom fields
      ])
    );
  }
}
