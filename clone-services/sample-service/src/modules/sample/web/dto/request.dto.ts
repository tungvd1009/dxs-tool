import { IsNotEmpty, IsString } from "class-validator";

export class CreateSampleRequestDto {}

export class UpdateSampleRequestDto {
  @IsString()
  @IsNotEmpty()
  id: string;
}
