import { Connection } from "mongoose";
import { SampleSchema } from "./schema";
import { CommonConst } from "../../shared/constant/index";

export const SampleProviders = [
  {
    provide: CommonConst.SAMPLE_QUERY_MODEL_TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(CommonConst.SAMPLE_COLLECTION, SampleSchema),
    inject: [CommonConst.QUERY_CONNECTION_TOKEN],
  },
];
