import * as mongoose from "mongoose";
import uuid = require("uuid");

export const SampleSchema = new mongoose.Schema({
  // Khai báo các field common
  _id: { type: String },
  description: { type: String, default: "" },
  createdBy: { type: String },
  createdDate: { type: Date, default: () => Date.now(), index: true }, // Đánh index
  modifiedBy: { type: String },
  modifiedDate: { type: Date, default: () => Date.now(), index: true }, // Đánh index
  id: { type: String, default: uuid.v4, index: true },
  active: { type: Boolean, default: true }, // Có public hay không?
  // Khai báo các field của riêng từng chức năng
});

SampleSchema.pre("save", function (next) {
  this._id = this.get("id");
  next();
});
