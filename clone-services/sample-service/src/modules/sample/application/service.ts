import { Injectable, BadRequestException } from "@nestjs/common";
import { SampleRepository } from "../infra/repository";
import {
  SampleResponseDto,
  SamplePagingResponseDto,
} from "../web/dto/response.dto";
import {
  CreateSampleRequestDto,
  UpdateSampleRequestDto,
} from "../web/dto/request.dto";
import { ErrorConst } from "../../shared/constant/error.const";
const uuid = require("uuid");

@Injectable()
export class SampleService {
  private readonly context = SampleService.name;

  constructor(private readonly repository: SampleRepository) {}

  async findPublicById(id: string): Promise<SampleResponseDto> {
    const model = await this.repository.findOne({
      id,
      active: true,
    });
    if (!model) {
      throw new BadRequestException({
        errors: ErrorConst.Error(ErrorConst.NOT_FOUND, "Sample"),
      });
    }
    return new SampleResponseDto(model);
  }

  async findPublicAll(query: any = {}): Promise<SamplePagingResponseDto> {
    const _query: any = {
      active: true,
    };
    if (query._fields) {
      _query._fields = query._fields;
    }
    if (query.q) {
      _query.$or = [{ name: { $regex: query.q, $options: "i" } }];
    }
    const page = query.page ? Number(query["page"]) : 1;
    const pageSize = query.pageSize ? Number(query["pageSize"]) : 10;
    _query.page = page;
    _query.pageSize = pageSize;
    _query.isPaging = true;
    const res = await Promise.all([
      await this.repository.findAll(_query),
      await this.repository.countAll(_query),
    ]);
    return new SamplePagingResponseDto({
      rows: res[0].map((model) => new SampleResponseDto(model)),
      total: res[1],
      page,
      pageSize,
      totalPages: Math.floor((res[1] + pageSize - 1) / pageSize),
    });
  }

  async findById(id: string): Promise<SampleResponseDto> {
    const model = await this.repository.findOne({ id });
    if (!model) {
      throw new BadRequestException({
        errors: ErrorConst.Error(ErrorConst.NOT_FOUND, "Sample"),
      });
    }
    return new SampleResponseDto(model);
  }

  async findAll(query: any = {}): Promise<SamplePagingResponseDto> {
    const _query: any = {};
    if (query._fields) {
      _query._fields = query._fields;
    }
    if (query.q) {
      _query.$or = [{ name: { $regex: query.q, $options: "i" } }];
    }
    if (query.user && query.user.id) {
      _query["createdBy"] = query.user.id;
    }
    const page = query.page ? Number(query["page"]) : 1;
    const pageSize = query.pageSize ? Number(query["pageSize"]) : 10;
    _query.page = page;
    _query.pageSize = pageSize;
    _query.isPaging = true;
    const res = await Promise.all([
      await this.repository.findAll(_query),
      await this.repository.countAll(_query),
    ]);
    return new SamplePagingResponseDto({
      rows: res[0].map((model) => new SampleResponseDto(model)),
      total: res[1],
      page,
      pageSize,
      totalPages: Math.floor((res[1] + pageSize - 1) / pageSize),
    });
  }

  async create(user: any, dto: CreateSampleRequestDto) {
    const model: any = { ...dto };
    if (model.id) {
      delete model.id;
    }
    model.id = uuid.v4();
    model.modifiedBy = user.id;
    model.createdBy = user.id;
    await this.repository.create(model);
    return { id: model.id };
  }

  async update(user: any, dto: UpdateSampleRequestDto) {
    const oldModel = await this.repository.findOne({ id: dto.id });
    if (!oldModel) {
      throw new BadRequestException({
        errors: ErrorConst.Error(ErrorConst.NOT_FOUND, "Sample"),
      });
    }
    const model = Object.assign(oldModel, dto);
    model.modifiedBy = user.id;
    await this.repository.patch(model.id, model);
    return { id: model.id };
  }

  async delete(user: any, id: string) {
    await this.repository.delete({ id });
    return { id: id };
  }
}
