import { QueryDatabaseModule } from "../database/query/query.database.module";
import { AuthModule } from "../auth/auth.module";
import { LoggerModule } from "../.././modules/logger/logger.module";
import { MgsSenderModule } from "../mgs-sender/mgs-sender.module";

import { Module } from "@nestjs/common";
import { SampleProviders } from "./infra/providers";
import { SampleRepository } from "./infra/repository";
import { SampleService } from "./application/service";
import { SampleAdminController } from "./web/admin-controller";
import { SampleAuthController } from "./web/auth-controller";
import { SamplePublicController } from "./web/public-controller";

@Module({
  imports: [QueryDatabaseModule, AuthModule, LoggerModule, MgsSenderModule],
  controllers: [
    SampleAdminController,
    SampleAuthController,
    SamplePublicController,
  ],
  providers: [...SampleProviders, SampleRepository, SampleService],
  exports: [SampleService],
})
export class SampleModule {}
