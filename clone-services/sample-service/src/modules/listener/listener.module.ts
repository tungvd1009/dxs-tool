import { Module } from "@nestjs/common";
import { ListenerController } from "./listener.controller";
import { SampleModule } from "../sample/module";

@Module({
  imports: [SampleModule],
  controllers: [ListenerController],
})
export class ListenerModule {}
