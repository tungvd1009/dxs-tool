export interface IBaseInterface {
  description: string;
  createdBy: string;
  createdDate: Date;
  modifiedBy: string;
  modifiedDate: Date;
  id: string;
  active: boolean;
}
