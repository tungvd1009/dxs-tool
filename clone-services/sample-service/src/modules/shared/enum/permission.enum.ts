export enum PermissionEnum {
  // projectLead
  PROJECTLEAD_CREATE = "projectLead.create",
  PROJECTLEAD_UPDATE = "projectLead.update",
  PROJECTLEAD_DELETE = "projectLead.delete",
  PROJECTLEAD_GET_ALL = "projectLead.get.all",
  PROJECTLEAD_GET_ID = "projectLead.get.id",
  //sample
  SAMPLE_CREATE = "sample.create",
  SAMPLE_UPDATE = "sample.update",
  SAMPLE_DELETE = "sample.delete",
  SAMPLE_GET_ALL = "sample.get.all",
  SAMPLE_GET_ID = "sample.get.id",
}
