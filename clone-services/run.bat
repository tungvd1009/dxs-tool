set CUR_DIR=%cd%
set PYTHON_PATH=C:\Users\ThinkPro\AppData\Local\Programs\Python\Python39\python.exe
set FILE_PY=clone-service.py
set SERVICE_TEMPLATE_DIR=sample-service
set SRC_MODULE_NAME=sample
set DST_MODULE_NAME=communication
set ADD_MODULE_NAME=orgchart
set IS_CLONE=1
if %IS_CLONE%==1 (
	call %PYTHON_PATH% %cd%\src\%FILE_PY% %cd%\%SERVICE_TEMPLATE_DIR% %SRC_MODULE_NAME% %ADD_MODULE_NAME% clone
) else (
	call %PYTHON_PATH% %cd%\src\%FILE_PY% %cd%\%SERVICE_TEMPLATE_DIR% %SRC_MODULE_NAME% %DST_MODULE_NAME% replace
)
npx prettier --write "%SERVICE_TEMPLATE_DIR%/**/*.ts"